# Program oblicza pole powierzchni dywanu Sierpinskiego o boku 27 i liczbie iteracji 3

class Carpet:
    """ Klasa zawiera metody do obliczania pola powierzchni dywanu Sierpinskiego o zadanym boku i liczbie iteracji"""
    
    def __init__(self, side_len):
        self.side_len = side_len
        self.field = self.side_len**2
        
    def __str__(self):
        return str(self.field)
        
    def carpet_rek(self,side_len, iteration):
        """ Oblicza pole powierzchni dywanu Sierpinskiego o zadanym boku i liczbie iteracji"""
        
        if iteration > 0:
            side_len = side_len / 3.0
            print "bok = " + str(side_len)
            print self.field
            self.field = self.field - (side_len * side_len)
            print self.field
            for i in range(8):
                self.carpet_rek(side_len, iteration - 1 )
                
            


f = Carpet(27)
f.carpet_rek(27,3)
print "Wynik = " + str(f)
